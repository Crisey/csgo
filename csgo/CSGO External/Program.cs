﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace CSGO_External
{
    class Program
    {
        static void Main(string[] args)
        {
            for (int i = 0; i < Logs.logWithIndex.Count(); i++)
            {
                Logs.logWithIndex[i] = new Logs.LogMessage();
            }

                Loop();
        }

        static void Loop()
        {
            while (!Memory.GetProcessWithDll("csgo", "client.dll", "engine.dll"))
            {
                Logs.AddLogIndex(0, 10, "CSGO Exists", "Launch your csgo.exe", true);
                Logs.ReadLogs(Logs.logWithIndex);
                Thread.Sleep(550);
                Logs.ClearLog(0, Logs.logWithIndex);
                Console.Clear();
            }

            LocalPlayer localPlayer = new LocalPlayer();
            
            while (true)
            {
                int hp =  WinApi.ReadInt(Memory.Offsets.iHealth + localPlayer.player);
                Logs.AddLogIndex(500, 3, "HP", "HP: " + hp, true);
                Thread.Sleep(10);
                Console.Clear();
                Logs.ReadLogs(Logs.logWithIndex);
            }
            
        }
    }
}
