﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CSGO_External
{
    static class Logs
    {
        public static bool adminRights = true;
        public static List<LogMessage> log = new List<LogMessage>();
        public static List<LogMessage> logWithIndex = new List<LogMessage>(new LogMessage[1000]);

        public class LogMessage
        {
            public int Power;
            public bool ForUser;
            public string Message = string.Empty;
            public string Title;
        }

        public static void AddLog(byte power, string title, string message, bool forUser)
        {
            log.Add(new LogMessage
            {
                Power = power,
                ForUser = forUser,
                Title = title,
                Message = message,
            });
        }

        public static void AddLogIndex(ushort index, byte power, string title, string message, bool forUser)
        {
            if (logWithIndex[index].Message == string.Empty)
                logWithIndex.Insert(index, new LogMessage
                {
                    Power = power,
                    ForUser = forUser,
                    Title = title,
                    Message = message,
                });
            else
            {
                logWithIndex.RemoveAt(index);
                logWithIndex.Insert(index, new LogMessage
                {
                    Power = power,
                    ForUser = forUser,
                    Title = title,
                    Message = message,
                });
            }
        }

        public static void ReadLog(short value, List<LogMessage> logs)
        {
            try
            {
                if ((logs[value].ForUser && logs[value].Message != string.Empty) || (!logs[value].ForUser && logs[value].Message != string.Empty && adminRights))
                {
                    Console.WriteLine("Log[{0}] | Power: " + logs[value].Power + " | Title:" + logs[value].Title + " | Message: " + logs[value].Message, value);
                }
                else if(logs[value].Message == string.Empty)
                { }
                else
                    Console.WriteLine("Log[{0}]. Error at logs title: " + logs[value].Title + "\nPlease contact with administrator", value);
            }
            catch (ArgumentOutOfRangeException ex)
            { Console.WriteLine(ex.Message); }
        }

        public static void ReadLogs(List<LogMessage> logs)
        {
            for (byte a = 0; a <= 10; a++)
            {
                for (short i = 0; i < logs.Count; i++)
                {
                    if (logs[i].Power == a)
                        ReadLog(i, logs);
                }
            }
        }

        public static void ClearLogs(List<LogMessage> logs)
        {
            for (int i = 0; i < logs.Count; i++)
            {
                logs[i] = new LogMessage
                {
                    Power = 0,
                    Message = string.Empty,
                    Title = string.Empty,
                    ForUser = false,
                };
            }
        }

        public static void ClearLog(short index, List<LogMessage> logs)
        {
            logs.Insert(index, logs[index] = new LogMessage{});
        }
    }
}
