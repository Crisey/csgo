﻿using System;
using System.Diagnostics;
using System.Runtime.InteropServices;

namespace CSGO_External
{
    class WinApi
    {
        [DllImport("User32.dll")]
        private extern static bool GetAsyncKeyState(int Key);

        [DllImport("kernel32.dll", SetLastError = true)]
        private static extern bool WriteProcessMemory(int hProcess, int lpBaseAddress, byte[] dbToWrite, UIntPtr nSize, IntPtr lpNumberOfBytesWritten);

        [DllImport("kernel32.dll")]
        private static extern bool ReadProcessMemory(int hProcess, int lpBaseAddress, [Out] byte[] lpBuffer, UIntPtr nSize, IntPtr lpNumberOfBytesRead);

        [DllImport("kernel32.dll")]
        public static extern int CloseHandle(int hProcess);

        [DllImport("kernel32.dll")]
        public static extern int OpenProcess(uint dwDesiredAccess, bool bInheritHandle, int dwProcessId);

        public enum ProcessAccess : uint
        {
            All = 0x001F0FFF,
            Terminate = 0x00000001,
            CreateThread = 0x00000002,
            VMOperation = 0x00000008,
            VMRead = 0x00000010,
            VMWrite = 0x00000020,
            DupHandle = 0x00000040,
            SetInformation = 0x00000200,
            QueryInformation = 0x00000400,
            Synchronize = 0x00100000
        }

        public static int ReadInt(long Address)
        {
            byte[] buffer = new byte[sizeof(int)];
            ReadProcessMemory(Memory.pHandle, (int)Address, buffer, (UIntPtr)4, IntPtr.Zero);
            return BitConverter.ToInt32(buffer, 0);
        }

        public static double ReadDouble(long Address)
        {
            byte[] buffer = new byte[sizeof(double)];
            ReadProcessMemory(Memory.pHandle, (int)Address, buffer, (UIntPtr)4, IntPtr.Zero);
            return BitConverter.ToDouble(buffer, 0);
        }

        public static double[] ReadDoubleArray(long Address)
        {
            byte[] buffer = new byte[sizeof(double)];
            double[] ret = new double[3];
            ReadProcessMemory(Memory.pHandle, (int)Address, buffer, (UIntPtr)(4), IntPtr.Zero);
            ret[0] = BitConverter.ToDouble(buffer, 0);
            ReadProcessMemory(Memory.pHandle, (int)Address + 4, buffer, (UIntPtr)(4), IntPtr.Zero);
            ret[1] = BitConverter.ToDouble(buffer, 0);
            ReadProcessMemory(Memory.pHandle, (int)Address + 8, buffer, (UIntPtr)(4), IntPtr.Zero);
            ret[2] = BitConverter.ToDouble(buffer, 0);

            return ret;
        }
    }
}
