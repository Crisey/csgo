﻿using System;

namespace CSGO_External
{
    class Player
    {
        public int iHealth = Memory.Offsets.iHealth;
        public int iTeamNum = Memory.Offsets.iTeamNum;
        public int m_bDormant = Memory.Offsets.m_bDormant;
        public int m_iID = Memory.Offsets.m_iID;

        public bool IsValid()
        {
            return (iTeamNum == 2 || iTeamNum == 3) && iHealth > 0 && m_bDormant != 1 && m_iID != 0;
        }
            
    }
}
