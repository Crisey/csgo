﻿using System;
using System.Diagnostics;
using System.Threading.Tasks;

namespace CSGO_External
{
    class Memory
    {
        public static int pHandle;
        public static int clientDll;
        public static int engineDll;

        public class Offsets
        {
            public static int weaponID =           0x168c;
            public static int hLastWeapon =        0x16b8;
            public static int hactiveWeaponb =     0x12c0;
            public static int vecPunch =           0x13e8;
            public static int nextPrimaryAttack =  0x159c;
            public static int flashDuration =      0x1db4;
            public static int fFlags =             0x100;
            public static int iHealth =            0xfc;
            public static int iTeamNum =           0xf0;
            public static int iShotsFired =        0x1d6c;
            public static int iCrosshairID =       0x2410;
            public static int m_dwClientState =    0x5ce294;
            public static int m_dwInGame =         0xe8;
            public static int localPlayer =        0xa74cdc;
            public static int entityList =         0x4a16c14;
            public static int m_bDormant =         0xe9;
            public static int m_iID =              0x64;
        }

        public static bool GetProcessWithDll(string ProcessName, string ClientDll, string EngineDll)
        {
            foreach(Process proc in Process.GetProcessesByName(ProcessName))
            {
                pHandle = WinApi.OpenProcess((uint)WinApi.ProcessAccess.VMRead, false, proc.Id);
                foreach(ProcessModule Module in proc.Modules)
                {
                    if(Module.ModuleName.Equals(clientDll))
                        clientDll = (int)Module.BaseAddress;
                    if(Module.ModuleName.Equals(EngineDll))
                        engineDll = (int)Module.BaseAddress;

                    return true;
                }
            }
            return false;
        }
    }
}
