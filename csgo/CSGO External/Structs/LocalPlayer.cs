﻿using System;

namespace CSGO_External
{
    class LocalPlayer
    {
        public int iHealth = Memory.Offsets.iHealth;
        public int iTeamNum = Memory.Offsets.iTeamNum;
        public int m_bDormant = Memory.Offsets.m_bDormant;
        public int m_iID = Memory.Offsets.m_iID;
        public int iCrosshair = Memory.Offsets.iCrosshairID;
        public int iShotsFired = Memory.Offsets.iShotsFired;
        public int fFlags = Memory.Offsets.fFlags;

        public int player = WinApi.ReadInt(Memory.clientDll + Memory.Offsets.localPlayer);

        public bool IsValid()
        {
            return (iTeamNum == 2 || iTeamNum == 3) && iHealth > 0 && m_iID != 0;
        }
    }
}
